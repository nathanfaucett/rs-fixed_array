use alloc::heap::Heap;
use alloc::allocator::{Alloc, Layout};
use alloc::boxed::Box;
use alloc::vec::Vec;

use core::intrinsics::assume;
use core::ptr::Unique;
use core::iter::FromIterator;
use core::{mem, slice, fmt};
use core::ops::*;

use data_structure_traits::*;


pub struct FixedArray<T, A = Heap>
    where A: Alloc,
{
    ptr: Unique<T>,
    len: usize,
    a: A,
}

impl<T: Default, A: Alloc> FixedArray<T, A> {

    #[inline(always)]
    pub fn set_defaults(&mut self) {
        for v in self {
            *v = T::default();
        }
    }
}

impl<I, T> From<I> for FixedArray<T>
    where I: IntoIterator<Item = T>,
          <I as IntoIterator>::IntoIter: ExactSizeIterator,
{
    #[inline]
    fn from(collection: I) -> Self {
        let iter = collection.into_iter();
        let mut fixed_array = Self::with_len(iter.len());
        let mut i = 0;

        for v in iter {
            fixed_array[i] = v;
            i += 1;
        }

        fixed_array
    }
}

impl<T, A: Alloc> FixedArray<T, A> {

    #[inline(always)]
    pub fn with_len_in(len: usize, a: A) -> Self {
        FixedArray::allocate_in(len, false, a)
    }

    #[inline(always)]
    pub fn with_len_zeroed_in(len: usize, a: A) -> Self {
        FixedArray::allocate_in(len, true, a)
    }

    #[inline]
    pub fn generate_len_in<F>(len: usize, f: F, a: A) -> Self
        where F: Fn(usize) -> T,
    {
        let mut fixed_array = FixedArray::allocate_in(len, false, a);
        let mut index = 0;

        for v in &mut fixed_array {
            *v = f(index);
            index += 1;
        }

        fixed_array
    }

    #[inline]
    fn allocate_in(len: usize, zeroed: bool, mut a: A) -> Self {
        unsafe {
            let elem_size = mem::size_of::<T>();

            let alloc_size = len.checked_mul(elem_size).expect("capacity overflow");
            alloc_guard(alloc_size);

            let ptr = if alloc_size == 0 {
                mem::align_of::<T>() as *mut u8
            } else {
                let align = mem::align_of::<T>();
                let result = if zeroed {
                    a.alloc_zeroed(Layout::from_size_align(alloc_size, align).unwrap())
                } else {
                    a.alloc(Layout::from_size_align(alloc_size, align).unwrap())
                };
                match result {
                    Ok(ptr) => ptr,
                    Err(error) => a.oom(error),
                }
            };

            FixedArray {
                ptr: Unique::new(ptr as *mut _).unwrap(),
                len: len,
                a: a,
            }
        }
    }

    #[inline]
    pub fn set_len(&mut self, new_len: usize) {
        unsafe {
            let elem_size = mem::size_of::<T>();

            assert!(elem_size != 0, "capacity overflow");

            let ptr_res = if self.len == 0 {
                let ptr_res = self.a.alloc_array::<T>(new_len);
                ptr_res
            } else {
                let new_alloc_size = new_len * elem_size;
                alloc_guard(new_alloc_size);
                let ptr_res = self.a.realloc_array(self.ptr, self.len, new_len);
                ptr_res
            };

            let unique = match ptr_res {
                Err(error) => self.a.oom(error),
                Ok(unique) => unique,
            };

            self.ptr = unique;
            self.len = new_len;
        }
    }

    #[inline(never)]
    #[cold]
    pub fn double(&mut self) {
        unsafe {
            let elem_size = mem::size_of::<T>();

            assert!(elem_size != 0, "capacity overflow");

            let (new_len, uniq) = match self.current_layout() {
                Some(cur) => {
                    let new_len = 2 * self.len;
                    let new_size = new_len * elem_size;
                    let new_layout = Layout::from_size_align_unchecked(new_size, cur.align());
                    alloc_guard(new_size);
                    let ptr_res = self.a.realloc(self.ptr.as_ptr() as *mut u8, cur, new_layout);
                    match ptr_res {
                        Ok(ptr) => (new_len, Unique::new_unchecked(ptr as *mut T)),
                        Err(e) => self.a.oom(e),
                    }
                }
                None => {
                    let new_len = if elem_size > (!0) / 8 { 1 } else { 4 };
                    match self.a.alloc_array::<T>(new_len) {
                        Ok(ptr) => (new_len, ptr),
                        Err(e) => self.a.oom(e),
                    }
                }
            };
            self.ptr = uniq;
            self.len = new_len;
        }
    }

    #[inline(never)]
    #[cold]
    pub fn double_in_place(&mut self) -> bool {
        unsafe {
            let elem_size = mem::size_of::<T>();
            let old_layout = match self.current_layout() {
                Some(layout) => layout,
                None => return false,
            };

            assert!(elem_size != 0, "capacity overflow");

            let new_len = 2 * self.len;
            let new_size = new_len * elem_size;

            alloc_guard(new_size);

            let ptr = self.as_ptr() as *mut _;
            let new_layout = Layout::from_size_align_unchecked(new_size, old_layout.align());

            match self.a.grow_in_place(ptr, old_layout, new_layout) {
                Ok(_) => {
                    self.len = new_len;
                    true
                },
                Err(_) => {
                    false
                },
            }
        }
    }

    #[inline]
    pub unsafe fn from_raw_parts_in(ptr: *mut T, len: usize, a: A) -> Self {
        FixedArray {
            ptr: Unique::new(ptr).unwrap(),
            len: len,
            a: a,
        }
    }

    #[inline]
    pub fn from_box_in(mut slice: Box<[T]>, a: A) -> Self {
        unsafe {
            let result = FixedArray::from_raw_parts_in(slice.as_mut_ptr(), slice.len(), a);
            mem::forget(slice);
            result
        }
    }

    #[inline]
    pub unsafe fn into_box(self) -> Box<[T]> {
        let slice = slice::from_raw_parts_mut(self.as_ptr(), self.len);
        let output: Box<[T]> = Box::from_raw(slice);
        mem::forget(self);
        output
    }

    #[inline(always)]
    pub fn as_ptr(&self) -> *mut T {
        self.ptr.as_ptr()
    }

    #[inline]
    pub fn as_slice(&self) -> &[T] {
        unsafe {
            let p = self.ptr.as_ptr();
            assume(!p.is_null());
            slice::from_raw_parts(p, self.len)
        }
    }
    #[inline]
    pub fn as_slice_mut(&mut self) -> &mut [T] {
        unsafe {
            let p = self.ptr.as_ptr();
            assume(!p.is_null());
            slice::from_raw_parts_mut(p, self.len)
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        if mem::size_of::<T>() == 0 {
            !0
        } else {
            self.len
        }
    }

    #[inline(always)]
    pub unsafe fn get_unchecked(&self, index: usize) -> &T {
        &*self.as_ptr().offset(index as isize)
    }
    #[inline(always)]
    pub unsafe fn get_unchecked_mut(&mut self, index: usize) -> &mut T {
        &mut *self.as_ptr().offset(index as isize)
    }

    #[inline(always)]
    pub fn get(&self, index: usize) -> Option<&T> {
        if index < self.len() {
            Some(unsafe {self.get_unchecked(index)})
        } else {
            None
        }
    }
    #[inline(always)]
    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        if index < self.len() {
            Some(unsafe {self.get_unchecked_mut(index)})
        } else {
            None
        }
    }

    #[inline(always)]
    pub fn alloc(&self) -> &A {
        &self.a
    }

    #[inline(always)]
    pub fn alloc_mut(&mut self) -> &mut A {
        &mut self.a
    }

    #[inline]
    fn current_layout(&self) -> Option<Layout> {
        if self.len == 0 {
            None
        } else {
            unsafe {
                let align = mem::align_of::<T>();
                let size = mem::size_of::<T>() * self.len;
                Some(Layout::from_size_align_unchecked(size, align))
            }
        }
    }

    #[inline]
    pub unsafe fn dealloc(&mut self) {
        let elem_size = mem::size_of::<T>();

        if elem_size != 0 && self.len != 0 {
            let ptr = self.as_ptr() as *mut u8;
            let layout = Layout::new::<T>().repeat(self.len).unwrap().0;
            self.a.dealloc(ptr, layout);
        }
    }
}

impl<T> FixedArray<T, Heap> {

    #[inline(always)]
    pub fn with_len(len: usize) -> Self {
        FixedArray::allocate_in(len, false, Heap)
    }

    #[inline(always)]
    pub fn with_len_zeroed(len: usize) -> Self {
        FixedArray::allocate_in(len, true, Heap)
    }

    #[inline(always)]
    pub fn generate_len<F>(len: usize, f: F) -> Self
        where F: Fn(usize) -> T,
    {
        FixedArray::generate_len_in(len, f, Heap)
    }

    #[inline(always)]
    pub unsafe fn from_raw_parts(ptr: *mut T, len: usize) -> Self {
        Self::from_raw_parts_in(ptr, len, Heap)
    }

    #[inline(always)]
    pub fn from_box(slice: Box<[T]>) -> Self {
        Self::from_box_in(slice, Heap)
    }
}

impl<T, A: Alloc> Drop for FixedArray<T, A> {

    #[inline(always)]
    fn drop(&mut self) {
        unsafe {
            self.dealloc();
        }
    }
}

impl<T, A: Alloc> Deref for FixedArray<T, A> {
    type Target = [T];

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}
impl<T, A: Alloc> DerefMut for FixedArray<T, A> {

    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_slice_mut()
    }
}

impl<T: Clone, A: Clone + Alloc> Clone for FixedArray<T, A> {

    #[inline]
    fn clone(&self) -> Self {
        let mut cloned = FixedArray::with_len_in(self.len, self.a.clone());
        {
            let slice: &mut [T] = &mut *cloned;
            let mut i = 0;

            for v in self {
                slice[i] = v.clone();
                i += 1;
            }
        }
        cloned
    }
}

impl<T, A: Alloc> Collection for FixedArray<T, A> {

    #[inline(always)]
    fn len(&self) -> usize {
        Self::len(self)
    }
}

impl<T, A: Alloc> CollectionMut for FixedArray<T, A> {

    #[inline(always)]
    fn clear(&mut self) {
        self.set_len(0);
    }
}

impl<T, A: Alloc> Get<usize> for FixedArray<T, A> {
    type Output = T;

    #[inline(always)]
    fn get(&self, index: usize) -> Option<&Self::Output> {
        FixedArray::get(self, index)
    }
}
impl<T, A: Alloc> GetMut<usize> for FixedArray<T, A> {

    #[inline(always)]
    fn get_mut(&mut self, index: usize) -> Option<&mut Self::Output> {
        FixedArray::get_mut(self, index)
    }
}

impl<T, A: Alloc> Index<usize> for FixedArray<T, A> {
    type Output = T;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        Index::index(&**self, index)
    }
}
impl<T, A: Alloc> IndexMut<usize> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        IndexMut::index_mut(&mut **self, index)
    }
}

impl<T, A: Alloc> Index<Range<usize>> for FixedArray<T, A> {
    type Output = [T];

    #[inline(always)]
    fn index(&self, index: Range<usize>) -> &[T] {
        Index::index(&**self, index)
    }
}
impl<T, A: Alloc> Index<RangeTo<usize>> for FixedArray<T, A> {
    type Output = [T];

    #[inline(always)]
    fn index(&self, index: RangeTo<usize>) -> &[T] {
        Index::index(&**self, index)
    }
}
impl<T, A: Alloc> Index<RangeFrom<usize>> for FixedArray<T, A> {
    type Output = [T];

    #[inline(always)]
    fn index(&self, index: RangeFrom<usize>) -> &[T] {
        Index::index(&**self, index)
    }
}
impl<T, A: Alloc> Index<RangeFull> for FixedArray<T, A> {
    type Output = [T];

    #[inline(always)]
    fn index(&self, _index: RangeFull) -> &[T] {
        self
    }
}
impl<T, A: Alloc> Index<RangeInclusive<usize>> for FixedArray<T, A> {
    type Output = [T];

    #[inline(always)]
    fn index(&self, index: RangeInclusive<usize>) -> &[T] {
        Index::index(&**self, index)
    }
}
impl<T, A: Alloc> Index<RangeToInclusive<usize>> for FixedArray<T, A> {
    type Output = [T];

    #[inline(always)]
    fn index(&self, index: RangeToInclusive<usize>) -> &[T] {
        Index::index(&**self, index)
    }
}

impl<T, A: Alloc> IndexMut<Range<usize>> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, index: Range<usize>) -> &mut [T] {
        IndexMut::index_mut(&mut **self, index)
    }
}
impl<T, A: Alloc> IndexMut<RangeTo<usize>> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, index: RangeTo<usize>) -> &mut [T] {
        IndexMut::index_mut(&mut **self, index)
    }
}
impl<T, A: Alloc> IndexMut<RangeFrom<usize>> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, index: RangeFrom<usize>) -> &mut [T] {
        IndexMut::index_mut(&mut **self, index)
    }
}
impl<T, A: Alloc> IndexMut<RangeFull> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, _index: RangeFull) -> &mut [T] {
        self
    }
}
impl<T, A: Alloc> IndexMut<RangeInclusive<usize>> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, index: RangeInclusive<usize>) -> &mut [T] {
        IndexMut::index_mut(&mut **self, index)
    }
}
impl<T, A: Alloc> IndexMut<RangeToInclusive<usize>> for FixedArray<T, A> {

    #[inline(always)]
    fn index_mut(&mut self, index: RangeToInclusive<usize>) -> &mut [T] {
        IndexMut::index_mut(&mut **self, index)
    }
}

impl<'a, T: 'a, A: Alloc> IntoIterator for &'a FixedArray<T, A> {
    type Item = &'a T;
    type IntoIter = slice::Iter<'a, T>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        (&**self).into_iter()
    }
}

impl<'a, T: 'a, A: Alloc> IntoIterator for &'a mut FixedArray<T, A> {
    type Item = &'a mut T;
    type IntoIter = slice::IterMut<'a, T>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        (&mut **self).into_iter()
    }
}

impl<T> FromIterator<T> for FixedArray<T> {

    #[inline(always)]
    fn from_iter<I>(iter: I) -> Self
        where I: IntoIterator<Item = T>,
    {
        let mut vec = Vec::new();

        for v in iter {
            vec.push(v);
        }

        Self::from(vec)
    }
}


impl<T: fmt::Debug, A: Alloc> fmt::Debug for FixedArray<T, A> {

    #[inline(always)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&**self, f)
    }
}

impl<T: fmt::Display + fmt::Debug, A: Alloc> fmt::Display for FixedArray<T, A> {

    #[inline(always)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&**self, f)
    }
}


#[inline]
fn alloc_guard(alloc_size: usize) {
    if mem::size_of::<usize>() < 8 {
        assert!(alloc_size <= ::core::isize::MAX as usize, "capacity overflow");
    }
}
