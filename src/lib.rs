#![feature(alloc)]
#![feature(allocator_api)]
#![feature(core_intrinsics)]
#![feature(inclusive_range)]
#![feature(unique)]
#![no_std]


extern crate alloc;

extern crate data_structure_traits;


mod fixed_array;


pub use fixed_array::FixedArray;
