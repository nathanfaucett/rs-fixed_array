extern crate data_structure_traits;

extern crate fixed_array;


use fixed_array::FixedArray;


#[test]
fn test_set_len() {
    let mut fixed_array = FixedArray::<usize>::with_len(2);
    fixed_array.set_defaults();

    fixed_array[0] = 1;
    fixed_array[1] = 2;

    assert_eq!(fixed_array[0], 1);
    assert_eq!(fixed_array[1], 2);
    assert_eq!(fixed_array.len(), 2);

    fixed_array.set_len(4);

    fixed_array[2] = 3;
    fixed_array[3] = 4;

    assert_eq!(fixed_array[0], 1);
    assert_eq!(fixed_array[1], 2);
    assert_eq!(fixed_array[2], 3);
    assert_eq!(fixed_array[3], 4);
    assert_eq!(fixed_array.len(), 4);

    fixed_array.set_len(2);

    assert_eq!(fixed_array[0], 1);
    assert_eq!(fixed_array[1], 2);
    assert_eq!(fixed_array.len(), 2);
}

#[test]
fn test_get() {
    let mut fixed_array = FixedArray::<usize>::with_len(5);
    fixed_array.set_defaults();

    assert_eq!(fixed_array[0], 0);
    assert_eq!(fixed_array[1], 0);
    assert_eq!(fixed_array[2], 0);
    assert_eq!(fixed_array[3], 0);
    assert_eq!(fixed_array[4], 0);
}
#[test]
fn test_get_mut() {
    let mut fixed_array = FixedArray::<usize>::with_len(5);
    fixed_array.set_defaults();

    fixed_array[0] = 1;
    fixed_array[1] = 2;
    fixed_array[2] = 3;
    fixed_array[3] = 4;
    fixed_array[4] = 5;

    assert_eq!(fixed_array[0], 1);
    assert_eq!(fixed_array[1], 2);
    assert_eq!(fixed_array[2], 3);
    assert_eq!(fixed_array[3], 4);
    assert_eq!(fixed_array[4], 5);
}

#[test]
fn test_get_clone_mut() {
    let mut a = FixedArray::<usize>::with_len(3);
    a.set_defaults();

    let mut b = a.clone();

    a[0] = 1;
    a[1] = 2;
    a[2] = 3;

    b[0] = 4;
    b[1] = 5;
    b[2] = 6;

    assert_eq!(a[0], 1);
    assert_eq!(a[1], 2);
    assert_eq!(a[2], 3);

    assert_eq!(b[0], 4);
    assert_eq!(b[1], 5);
    assert_eq!(b[2], 6);
}

#[derive(Debug, PartialEq, Eq)]
enum Enum {
    Empty,
    Full
}

impl Default for Enum {
    fn default() -> Self {
        Enum::Empty
    }
}

#[test]
fn test_empty_get() {
    let mut fixed_array = FixedArray::<Enum>::with_len(3);
    fixed_array.set_defaults();

    assert_eq!(fixed_array[0], Enum::Empty);
    assert_eq!(fixed_array[1], Enum::Empty);
    assert_eq!(fixed_array[2], Enum::Empty);
}
#[test]
fn test_empty_get_mut() {
    let mut fixed_array = FixedArray::<Enum>::with_len(5);
    fixed_array.set_defaults();

    fixed_array[0] = Enum::Full;
    fixed_array[1] = Enum::Full;
    fixed_array[2] = Enum::Full;

    assert_eq!(fixed_array[0], Enum::Full);
    assert_eq!(fixed_array[1], Enum::Full);
    assert_eq!(fixed_array[2], Enum::Full);
}

#[test]
fn test_iter() {
    let mut fixed_array = FixedArray::<usize>::with_len(5);
    fixed_array.set_defaults();

    for value in fixed_array.iter() {
        assert_eq!(*value, 0);
    }
}
#[test]
fn test_iter_mut() {
    let mut fixed_array = FixedArray::<usize>::with_len(5);
    fixed_array.set_defaults();

    for value in fixed_array.iter_mut() {
        *value = 1;
    }
    for value in fixed_array.iter() {
        assert_eq!(*value, 1);
    }
}

#[test]
fn test_from() {
    let slice = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let fixed_array = FixedArray::from(slice.to_vec());
    assert_eq!(&*fixed_array, &slice);
}

#[test]
fn test_generate_len() {
    let fixed_array = FixedArray::generate_len(3, |i| i % 2 == 0);
    assert_eq!(&*fixed_array, &[true, false, true]);
}
