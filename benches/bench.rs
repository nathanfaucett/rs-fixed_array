#![feature(test)]


extern crate test;

extern crate fixed_array;


use test::Bencher;

use fixed_array::FixedArray;


const SIZE: usize = 1024 * 8;


#[bench]
fn bench_fixed_array(b: &mut Bencher) {
    let mut fixed_array = FixedArray::with_len(SIZE);

    fixed_array.set_defaults();

    b.iter(move || {
        for i in 0..SIZE {
            fixed_array[i] = i;
        }
        for i in 0..SIZE {
            assert_eq!(fixed_array[i], i);
        }
    });
}
#[bench]
fn bench_slice(b: &mut Bencher) {
    let mut fixed_array = [0usize; SIZE];

    b.iter(|| {
        for i in 0..SIZE {
            fixed_array[i] = i;
        }
        for i in 0..SIZE {
            assert_eq!(fixed_array[i], i);
        }
    });
}
#[bench]
fn bench_vec(b: &mut Bencher) {
    let mut fixed_array = [0usize; SIZE].to_vec();

    b.iter(|| {
        for i in 0..SIZE {
            fixed_array[i] = i;
        }
        for i in 0..SIZE {
            assert_eq!(fixed_array[i], i);
        }
    });
}
